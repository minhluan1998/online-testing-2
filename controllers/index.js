import FillInBlank from "./../models/fillInBlank.js";
import MultipleChoice from "./../models/multipleChoice.js";

const questionList = [];

// Lấy data từ DB
// arrow function ko làm thay đổi ngữ cảnh con trỏ this, còn function thì thay đổi ngữ cảnh con trỏ this
const getData = () => {
    axios({
        method: "GET",
        url: "/DeThiTracNghiem.json"
    })
        .then(res => {
            mapDataToObject(res.data);
            // map(): map mảng này sang mảng khác với cùng số lượng phần tử
            var contentArr = questionList.map((currentQuestion, i) => {
                return currentQuestion.content;
            })
            document.getElementById("content").innerHTML = renderQuestion();
        })
        .catch(err => {
            console.log(err);
        });
};
const mapDataToObject = data => {
    for (let question of data) {
        // destructuring
        const { questionType, _id, content, answers } = question;

        let newQuestion = {};
        if (question.questionType === 1) {
            newQuestion = new MultipleChoice(
                questionType,
                _id,
                content,
                answers
            );
        } else {
            newQuestion = new FillInBlank(questionType, _id, content, answers);
        }
        questionList.push(newQuestion);
    }
};

const renderQuestion = () => {
    let content = "";

    for (let i in questionList) {
        content += questionList[i].render(i - -1);
    }
    return content;
};
getData();
document.getElementById("btnSubmit").addEventListener("click", () => {
    let result = questionList.reduce((sum, currentQuestion, index) => {
        // ham reduce cua es6
        return (sum += currentQuestion.checkExact()); // phai return moi su dung duoc
    }, 0); // 0 là tham số khởi tạo của biến 0
    console.log(result);
});
// promise: khái niệm mới của ES6, có 3 trạng thái: pending, resolve, reject
// axios trả về 1 promise

// var a = {hoten: "Luan"};
// var b = {...a} // deep copy

// 3 ham cua es6
/*
map(), filter(), reduce()
*/
function clickDiEm() {
    // loc questiontype == 1
    var multipleChoices = questionList.filter((currentQuestion, index) => {
        return currentQuestion.questionType === 1;
    });
    console.log(multipleChoices);
}
