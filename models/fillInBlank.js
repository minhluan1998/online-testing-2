import Question from "./question.js";
// import abc from "./question.js";
// import obj from "./question"

class FillInBlank extends Question {
    // ...agrs là rest khi nằm trên tham số đầu vào, là spread khi ko nằm ở đó
    constructor(...args) {
        // spread operator
        // console.log(...args);
        super(...args);
    }

    render(index) {
        return `
            <div>
                <h4>Câu hỏi ${index}: ${this.content}</h4>
                <input type="text" id="question-${this._id}" />
            </div>
        `;
    }
    checkExact() {
        var input = document.getElementById("question-" + this._id);
        // console.log(input);
        var trueAnswer = this.answers[0].content.trim().toLowerCase();
        var inputValue = input.value.trim().toLowerCase();
        // console.log(trueAnswer === inputValue);
        return trueAnswer === inputValue;
    }
}
// console.log(new FillInBlank());

export default FillInBlank;
// console.log(calcSum());
/*

export bên class Question là export định danh
cái được export ra là object 
{
    Quesition: Question
}

*/
