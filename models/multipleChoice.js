import Question from "./question.js";
// import { Question } from './question';

class MultipleChoice extends Question {
    constructor(...args) {
        super(...args);
        // super(questionType, _id, content, answer)
    }

    renderAnswer() {
        let answerText = "";
        for (let ans of this.answers) {
            answerText += `
                <div>
                    <input type="radio" class="question-${this._id}" value="${
                ans._id
            }" name="${this._id}">
                    <span>${ans.content}</span>
                </div>
            `;
        }
        return answerText;
    }

    render(index) {
        return `
            <div>
                <h4>Câu hỏi ${index}: ${this.content}</h4>
                ${this.renderAnswer()}
            </div>
        `;
    }
    getExactId() {
        this.answers.forEach(ans => {
            if (ans.exact) {
                return ans._id;
            }
        });
    }
    checkExact() {
        // document.getElementsByClassName('question-' + this._id).forEach(answer => {
        //     console.log(answer);
        // })

        // var inputs = document.getElementsByClassName("question-" + this._id);
        // for (let i = 0; i < inputs.length; i++) {
        //     console.log(inputs[i]);
        // }
        let isExact = false;
        [...document.getElementsByClassName("question-" + this._id)].forEach(
            ans => {
                if (ans.checked) {
                    var ansId = ans.value;
                    const currentAnswer =
                        this.answers.find(ans => {
                            return ans._id === ansId.toString();
                        }) || {}; // falsy, tranh crash ung dung. neu bi undefine thi chuyen qua ben kia ||
                    isExact = currentAnswer.exact || false;
                    //  isExact = (currentAnswer.exact === 'true') || false;
                }
            }
        );
        return isExact;
    }
}

export default MultipleChoice;
/*
var: được phép khai báo 2 biến giống nhau trong cùng 1 khu vực
let: không được phép như vậy

var: phạm vi sử dụng trong function
let: phạm vi sử dụng trong cặp ngoặc nhọn

*/
